package py.com.linco.talisman.models;

public class VerificarCarton {
    private String verificar;

    private String verificarxFecha;

    public String getVerificar() {
        return verificar;
    }

    public void setVerificar(String verificar) {
        this.verificar = verificar;
    }

    public String getVerificarxFecha() {
        return verificarxFecha;
    }

    public void setVerificarxFecha(String verificarxFecha) {
        this.verificarxFecha = verificarxFecha;
    }
}
