package py.com.linco.talisman.models.usuario;

/**
 * Created by william on 12/22/17.
 */
public class ResultLogin {
    private Usuario usuario;

    private String status;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
