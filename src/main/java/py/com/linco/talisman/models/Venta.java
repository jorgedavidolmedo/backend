package py.com.linco.talisman.models;

/**
 * Created by william on 10/30/17.
 */
public class Venta {

    private String tipo;
    private String monto;
    private String token;
    private String idJugada;
    private String nroCarton;


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getNroCarton() {
        return nroCarton;
    }

    public void setNroCarton(String nroCarton) {
        this.nroCarton = nroCarton;
    }
}
