package py.com.linco.talisman.models.usuario;

/**
 * Created by william on 12/22/17.
 */
public class Usuario {
    private String apellido;

    private String nombre;

    private String clienteId;

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClienteId() {
        return clienteId;
    }

    public void setClienteId(String clienteId) {
        this.clienteId = clienteId;
    }
}
