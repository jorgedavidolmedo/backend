package py.com.linco.talisman.models;

/**
 * Created by william on 1/24/18.
 */
public class RespuestaGenerica {
    private String mensaje;

    public RespuestaGenerica(String mensaje){
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
