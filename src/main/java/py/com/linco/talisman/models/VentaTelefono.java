package py.com.linco.talisman.models;

/**
 * Created by william on 1/16/18.
 */
public class VentaTelefono {
    private String tipo;
    private String monto;
    private String token;
    private String idJugada;
    private String nroCarton;
    private String nroTelefono;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getNroCarton() {
        return nroCarton;
    }

    public void setNroCarton(String nroCarton) {
        this.nroCarton = nroCarton;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }
}
