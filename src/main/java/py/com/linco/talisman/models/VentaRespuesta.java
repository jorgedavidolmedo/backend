package py.com.linco.talisman.models;

/**
 * Created by william on 10/30/17.
 */
public class VentaRespuesta {

    private String url;

    public VentaRespuesta(String url){
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
