/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author william
 */
public class PremiosPorCarton {
    
    @JsonProperty("IdJugada")
    private String idJugada;
    
    @JsonProperty("NroPremio")
    private String nroPremio;

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getNroPremio() {
        return nroPremio;
    }

    public void setNroPremio(String nroPremio) {
        this.nroPremio = nroPremio;
    }
    
    
}
