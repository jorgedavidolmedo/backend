/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author william
 */
public class ConsultaCarton {
    
    @JsonProperty("IdJugada")
    private String idJugada;
    
    @JsonProperty("NroCarton")
    private String nroCarton; 

    
    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getNroCarton() {
        return nroCarton;
    }

    public void setNroCarton(String NroCarton) {
        this.nroCarton = NroCarton;
    }
    
    
}
