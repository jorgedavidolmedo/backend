package py.com.linco.talisman.models;

/**
 * Created by william on 2/1/18.
 */
public class CartonSemana {

    private String front;
    private String back;

    public String getFront() {
        return front;
    }

    public void setFront(String front) {
        this.front = front;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }
}
