/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;

/**
 *
 * @author william
 */
public class Bolilla {
    
    @JsonProperty("IdJugada")
    private String idJugada;

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }
    
    
    
}
