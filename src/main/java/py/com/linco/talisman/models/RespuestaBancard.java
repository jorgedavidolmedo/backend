package py.com.linco.talisman.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * Created by william on 11/17/17.
 */

@Entity(name = "respuestas_bancard")
public class RespuestaBancard implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "process_id")
    @NotNull
    private String processId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @NotNull
    private Integer cliente;

    @NotNull
    private Integer jugada;

    @NotNull
    private String carton;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getCliente() {
        return cliente;
    }

    public void setCliente(Integer cliente) {
        this.cliente = cliente;
    }

    public Integer getJugada() {
        return jugada;
    }

    public void setJugada(Integer jugada) {
        this.jugada = jugada;
    }

    public String getCarton() {
        return carton;
    }

    public void setCarton(String carton) {
        this.carton = carton;
    }

    @PrePersist
    public void prePersist(){
        this.fecha = new Date();
    }
}
