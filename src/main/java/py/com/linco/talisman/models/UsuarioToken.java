package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by william on 12/22/17.
 */
@Entity(name = "usuario_token")
public class UsuarioToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    @JsonIgnore
    private Integer idUsuario;

    @Column(name = "id_usuario_talisman")
    @JsonIgnore
    private Integer idUsuarioTalisman;

    @Column(name = "token")
    private String token;

    @Column(name = "email")
    private String email;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuarioTalisman() {
        return idUsuarioTalisman;
    }

    public void setIdUsuarioTalisman(Integer idUsuarioTalisman) {
        this.idUsuarioTalisman = idUsuarioTalisman;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
