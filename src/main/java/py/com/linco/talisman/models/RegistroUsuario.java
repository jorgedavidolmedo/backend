/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author william
 */

@JsonIgnoreProperties(value = {"numseg","fechanacimiento","direccion","Token","dirip","notif","news"},allowGetters = true)
public class RegistroUsuario {
    
    @JsonProperty("NameUser")
    private String nombreUsuario;
    
    @JsonProperty("Passw")
    private String password;
    
    @JsonProperty("NumSeg")
    private String numseg=null;
    
    @JsonProperty("ClieDoc")
    private String doc;
    
    @JsonProperty("ClieTipoDoc")
    private String tipoDoc;
    
    @JsonProperty("ClieName")
    private String nombre;
    
    @JsonProperty("ClieApe")
    private String apellido; 
    
    @JsonProperty("ClieFecNac")
    private String fechaNacimiento=null;
    
    @JsonProperty("ClieCorreo")
    private String correo;
    
    @JsonProperty("ClieNumCel")
    private String cel;
    
    @JsonProperty("ClieDireccion")
    private String direccion=null;
    
    @JsonProperty("Dpto")
    private Integer dpto;
    
    @JsonProperty("Ciudad")
    private Integer ciudad;
    
    @JsonProperty("Token")
    private String token;
    
    @JsonProperty("dirip")
    private String dirIp; 
    
    @JsonProperty("ClieNotif")
    private Integer notif=0; 
    
    @JsonProperty("ClieNews")
    private Integer news=0;

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumseg() {
        return numseg;
    }

    public void setNumseg(String numseg) {
        this.numseg = numseg;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getDpto() {
        return dpto;
    }

    public void setDpto(Integer dpto) {
        this.dpto = dpto;
    }

    public Integer getCiudad() {
        return ciudad;
    }

    public void setCiudad(Integer ciudad) {
        this.ciudad = ciudad;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDirIp() {
        return dirIp;
    }

    public void setDirIp(String dirIp) {
        this.dirIp = dirIp;
    }

    public Integer getNotif() {
        return notif;
    }

    public void setNotif(Integer notif) {
        this.notif = notif;
    }

    public Integer getNews() {
        return news;
    }

    public void setNews(Integer news) {
        this.news = news;
    }
    
    
    
}
