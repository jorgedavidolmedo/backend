/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 *
 * @author william
 */
public class Cartones {
    
    @JsonProperty("IdJugada")
    private String idJugada;
    
    @JsonProperty("Token")
    private String token;
    
    @JsonProperty("dirip")
    private String dirIp;
    
    @JsonProperty("TipoBusqueda")
    private String tipoBusqueda;
    
    @JsonProperty("Terminacion")
    private String terminacion;

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDirIp() {
        return dirIp;
    }

    public void setDirIp(String dirIp) {
        this.dirIp = dirIp;
    }

    public String getTipoBusqueda() {
        return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
        this.tipoBusqueda = tipoBusqueda;
    }

    public String getTerminacion() {
        return terminacion;
    }

    public void setTerminacion(String terminacion) {
        this.terminacion = terminacion;
    }
    
    
    
}
