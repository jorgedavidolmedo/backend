package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;

//Created by Vianella on 25/05/2018

public class EditarCliente {
	@JsonProperty("ClienteId")
	private String clienteId;
	
	@JsonProperty("ClieName")
	private String clieName;
	
	@JsonProperty("ClieApe")
	private String clieApe;

	@JsonProperty("fecNac")
	private FechaNac fecNac;

	@JsonProperty("ClieTipoDoc")
	private String clieTipoDoc;

	@JsonProperty("ClieDoc")
	private String clieDoc;

	@JsonProperty("ClieDireccion")
	private String cliDireccion;

	@JsonProperty("Dpto")
	private String dpto;

	@JsonProperty("Ciudad")
	private String ciudad;

	@JsonProperty("ClieCorreo")
	private String clieCorreo;

	@JsonProperty("email_check")
	private String emailCheck;

	@JsonProperty("ClieNumCel")
	private String clieNumCel;

	private Boolean newsletter = false;

	private String pass;

	@JsonProperty("pass_check")
	private String passCheck;

	public FechaNac getFecNac() {
		return fecNac;
	}

	public void setFecNac(FechaNac fecNac) {
		this.fecNac = fecNac;
	}

	public String getClieName() {
		return clieName;
	}

	public void setClieName(String clieName) {
		this.clieName = clieName;
	}

	public String getClieApe() {
		return clieApe;
	}

	public void setClieApe(String clieApe) {
		this.clieApe = clieApe;
	}

	public String getClieTipoDoc() {
		return clieTipoDoc;
	}

	public void setClieTipoDoc(String clieTipoDoc) {
		this.clieTipoDoc = clieTipoDoc;
	}

	public String getClieDoc() {
		return clieDoc;
	}

	public void setClieDoc(String clieDoc) {
		this.clieDoc = clieDoc;
	}

	public String getCliDireccion() {
		return cliDireccion;
	}

	public void setCliDireccion(String cliDireccion) {
		this.cliDireccion = cliDireccion;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getClieCorreo() {
		return clieCorreo;
	}

	public void setClieCorreo(String clieCorreo) {
		this.clieCorreo = clieCorreo;
	}

	public String getEmailCheck() {
		return emailCheck;
	}

	public void setEmailCheck(String emailCheck) {
		this.emailCheck = emailCheck;
	}

	public Boolean getNewsletter() {
		return newsletter;
	}

	public void setNewsletter(Boolean newsletter) {
		this.newsletter = newsletter;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPassCheck() {
		return passCheck;
	}

	public void setPassCheck(String passCheck) {
		this.passCheck = passCheck;
	}
	
	public String getClienteId() {
		return clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}
	
	class FechaNac {
		private String dia;
		private String mes;
		private String ano;

		public FechaNac() {

		}

		public String getDia() {
			return dia;
		}

		public void setDia(String dia) {
			this.dia = dia;
		}

		public String getMes() {
			return mes;
		}

		public void setMes(String mes) {
			this.mes = mes;
		}

		public String getAno() {
			return ano;
		}

		public void setAno(String ano) {
			this.ano = ano;
		}
	}
}