/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author william
 */
public class ProximoSorteo {
    @JsonProperty("Token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String Token) {
        this.token = Token;
    }
    
    
}
