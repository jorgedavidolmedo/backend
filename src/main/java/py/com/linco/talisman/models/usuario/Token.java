package py.com.linco.talisman.models.usuario;

/**
 * Created by william on 1/9/18.
 */
public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
