package py.com.linco.talisman.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.UsuarioToken;
import py.com.linco.talisman.models.usuario.Token;
import py.com.linco.talisman.models.usuario.Usuario;
import py.com.linco.talisman.repositories.UsuarioTokenRepository;

/**
 * Created by william on 1/9/18.
 */
@RestController
public class CartonesUsuario {
    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UsuarioTokenRepository usuarioTokenRepository;

    @PostMapping(value = "/api/cartones-usuario",produces = "application/json")
    public String getCartonesByUser(@RequestBody Token token){
        String url = new StringBuilder().append(propertiesConfig.getCartonesUrl()).toString();
        UsuarioToken usuario = usuarioTokenRepository.findByToken(token.getToken());
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("ClienteId", usuario.getIdUsuarioTalisman().toString());
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
        return response.getBody();
    }
}
