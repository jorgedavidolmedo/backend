/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.ConsultaCarton;
import py.com.linco.talisman.models.RespuestaBancard;
import py.com.linco.talisman.repositories.RespuestaBancardRepository;

/**
 *
 * @author william
 */
@RestController
public class MainController {
    
    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RespuestaBancardRepository respuestaBancardRepository;
    
    @GetMapping("/")
    public String raiz(){
        return "Talisman API Nuevo2";
    }
      
    @GetMapping("mi-ip")
    public Object getMyIp() throws URISyntaxException{
        RestTemplate restTemplate = new RestTemplate();
        Object object = restTemplate.getForObject(new URI("http://api.ipify.org/?format=json"), Object.class);
        return object;
    }
    
    @GetMapping("/test")
    public ConsultaCarton getroot() throws URISyntaxException{
        ConsultaCarton carton = new ConsultaCarton();
        carton.setIdJugada("1");
        carton.setNroCarton("12345");
        return carton;
    }

    @GetMapping("/todos")
    public Iterable<RespuestaBancard> getAll(){
        return respuestaBancardRepository.findAll();
    }
}
