/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.AciertoJugada;
import py.com.linco.talisman.models.Bolilla;
import py.com.linco.talisman.models.ConsultaCarton;

/**
 *
 * @author william
 */
@RestController
public class BolillasController {

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping("/api/bolillas-extraidas")
    public Object postCartonPremiado(@RequestBody Bolilla bolilla) throws URISyntaxException {
        String url = new StringBuilder()
                .append(propertiesConfig.getBaseUrl())
                .append("bolillas_extraidas.php").toString();
        return restTemplate.postForObject(new URI(url), bolilla, Object.class);
    }
    
    @PostMapping("api/aciertos")
    public Object getAciertos(@RequestBody AciertoJugada aciertoJugada){
        String url = new StringBuilder()
                        .append(propertiesConfig.getPruebasUrl())
                        .append("aciertos_por_jugadas.php").toString();
        return restTemplate.postForObject(url, aciertoJugada, Object.class);
    }

}
