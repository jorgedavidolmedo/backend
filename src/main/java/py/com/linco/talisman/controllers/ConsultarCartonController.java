/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.ConsultaCarton;
import py.com.linco.talisman.models.VerificarCarton;

/**
 *
 * @author william
 */
@RestController
public class ConsultarCartonController {
    
    @Autowired
    private PropertiesConfig propertiesConfig;
    
    @Autowired
    private RestTemplate restTemplate;
    
    @GetMapping("/api/sorteo-vigente")
    public Object getSorteoVigente() throws URISyntaxException{
        String url = new StringBuilder()
                        .append(propertiesConfig.getBaseUrl())
                        .append("sorteo_vigente.php").toString();
        return restTemplate.postForObject(new URI(url), null, Object.class);
    }
    
    @PostMapping("/api/buscar-carton-premiado")
    public Object postCartonPremiado(@RequestBody ConsultaCarton consultaCarton) throws URISyntaxException{
          String url = new StringBuilder()
                        .append(propertiesConfig.getAppBaseUrl())
                        .append("buscar_carton_premiado.php").toString();
        ResponseEntity<String> response = restTemplate.postForEntity( url, consultaCarton , String.class );
        return response.getBody();
    }
    
}
