/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.ProximoSorteo;

/**
 *
 * @author william
 */
@RestController
public class ProximoSorteoController {
    
    @Autowired
    private PropertiesConfig propertiesConfig;
    
    @Autowired
    private RestTemplate restTemplate;
    
    @GetMapping("/api/proximo-sorteo")
    public Object getSorteoVigente() throws URISyntaxException{
        String url = new StringBuilder()
                        .append(propertiesConfig.getBaseUrl())
                        .append("proximo_sorteo.php").toString();
        ProximoSorteo proximoSorteo = new ProximoSorteo();
        proximoSorteo.setToken(propertiesConfig.getToken());
        return restTemplate.postForObject(new URI(url), proximoSorteo, Object.class);
    }
    
}
