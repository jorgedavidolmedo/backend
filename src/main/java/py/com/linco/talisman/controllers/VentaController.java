package py.com.linco.talisman.controllers;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.JugadaHelper;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.*;
import py.com.linco.talisman.repositories.RespuestaBancardRepository;
import py.com.linco.talisman.repositories.UsuarioTokenRepository;
import py.com.linco.talisman.services.ServicePagoPersonal;

/**
 * Created by william on 10/30/17.
 */
@RestController
public class VentaController {

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RespuestaBancardRepository respuestaBancardRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UsuarioTokenRepository usuarioTokenRepository;

    @Autowired
    private JugadaHelper jugadaHelper;

    @Autowired
    private ServicePagoPersonal servicePagoPersonal;

    @PostMapping(value = "api/vender-carton/tigo")
    public ResponseEntity venderTigo(@RequestBody VentaTelefono venta){
        UsuarioToken usuarioToken = usuarioTokenRepository.findByToken(venta.getToken());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("PayType","tigo_money");
        map.add("amount",venta.getMonto());
        map.add("cambioUSD","1");
        map.add("ClienteId",usuarioToken.getIdUsuarioTalisman().toString());
        map.add("is_mobile","true");
        map.add("IdJugada",jugadaHelper.getIdJugada());
        map.add("NroCarton",venta.getNroCarton());
        map.add("nroTelefono",venta.getNroTelefono());

        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String url = new StringBuilder()
                .append(propertiesConfig.getVentaUrl()).toString();
        try{
            String res = restTemplate.postForObject(url,request,String.class);
            return new ResponseEntity(res, HttpStatus.ACCEPTED);
        }catch (HttpClientErrorException e){
            return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "api/vender-carton/personal")
    public ResponseEntity venderPersonal(@RequestBody VentaTelefono venta) throws InterruptedException {
        UsuarioToken usuarioToken = usuarioTokenRepository.findByToken(venta.getToken());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("PayType","personal");
        map.add("amount",venta.getMonto());
        map.add("cambioUSD","1");
        map.add("ClienteId",usuarioToken.getIdUsuarioTalisman().toString());
        map.add("is_mobile","true");
        map.add("IdJugada",jugadaHelper.getIdJugada());
        map.add("NroCarton",venta.getNroCarton());
        map.add("nroTelefono",venta.getNroTelefono());

        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String url = new StringBuilder()
                .append(propertiesConfig.getVentaUrl()).toString();
        servicePagoPersonal.sendPayRequest(url,request);

        return new ResponseEntity(new RespuestaGenerica("Pago en proceso"), HttpStatus.ACCEPTED);
    }
    @PostMapping(value = "api/vender-carton/personal-sync")
    public ResponseEntity venderPersonalSync(@RequestBody VentaTelefono venta){
        UsuarioToken usuarioToken = usuarioTokenRepository.findByToken(venta.getToken());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("PayType","personal");
        map.add("amount",venta.getMonto());
        map.add("cambioUSD","1");
        map.add("ClienteId",usuarioToken.getIdUsuarioTalisman().toString());
        map.add("is_mobile","true");
        map.add("IdJugada",jugadaHelper.getIdJugada());
        map.add("NroCarton",venta.getNroCarton());
        map.add("nroTelefono",venta.getNroTelefono());

        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String url = new StringBuilder()
                .append(propertiesConfig.getVentaUrl()).toString();
        try{
            String res = restTemplate.postForObject(url,request,String.class);
            return new ResponseEntity(res, HttpStatus.ACCEPTED);
        }catch (HttpClientErrorException e){
            return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/api/vender-carton/bancard")
    public ResponseEntity vender(@RequestBody Venta venta){
        UsuarioToken usuarioToken = usuarioTokenRepository.findByToken(venta.getToken());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("PayType","bancard");
        map.add("amount",jugadaHelper.getMontoJugada());
        map.add("cambioUSD","1");
        map.add("ClienteId",usuarioToken.getIdUsuarioTalisman().toString());
        map.add("is_mobile","true");
        map.add("IdJugada",jugadaHelper.getIdJugada());
        map.add("NroCarton",venta.getNroCarton());

        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String url = new StringBuilder()
                        .append(propertiesConfig.getVentaUrl()).toString();
        String res = restTemplate.postForObject(url,request,String.class);
        String processId = res.substring(res.indexOf("=")+1);

        return new ResponseEntity(new VentaRespuesta(res), HttpStatus.ACCEPTED);
    }

    @PostMapping(value = "api/pago-bancard")
    public ResponseEntity savePagoBancard(@RequestBody RespuestaBancard respuestaBancard){
        try{
            respuestaBancard = respuestaBancardRepository.save(respuestaBancard);
            return new ResponseEntity(HttpStatus.ACCEPTED);
        }catch (Exception e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }
}
