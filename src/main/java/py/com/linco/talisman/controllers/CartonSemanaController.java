/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.CartonSemana;

/**
 *
 * @author william
 */
@RestController
public class CartonSemanaController {
    @Autowired
    private PropertiesConfig propertiesConfig;
    
    @Autowired
    private RestTemplate restTemplate;
    
    
    @GetMapping("/api/carton-semana")
    public Object getCartonSemana() throws URISyntaxException, MalformedURLException, IOException{
             String url = new StringBuilder()
                        .append(propertiesConfig.getWebUrl())
                        .append("CartonesMobile/")
                        .append("cartones.php").toString();
        return restTemplate.getForObject(new URI(url), Object.class);

        
    }


    @GetMapping("/api/carton-semana/f")
    public Object getCarton() throws URISyntaxException {
        String url = new StringBuilder()
                .append(propertiesConfig.getAppUrl()).toString();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap map = new LinkedMultiValueMap();
        map.add("principal","OK");

        HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);



        String res =  restTemplate.postForObject(new URI(url),request, String.class);

        JSONObject jsonObject = new JSONObject(res);
        JSONArray jsonArray = jsonObject.getJSONArray("cartonsemana");
        JSONObject jsonObjectCarton = jsonArray.getJSONObject(0);
        String front = jsonObjectCarton.getString("front_src");
        String back = jsonObjectCarton.getString("back_src");

        CartonSemana cartonSemana = new CartonSemana();
        cartonSemana.setFront(front);
        cartonSemana.setBack(back);

        return cartonSemana;
    }

    @GetMapping("/api/carton-semana/v2")
    public String getCartonV2(){
        String url = new StringBuilder()
                        .append(propertiesConfig.getAppBaseUrl())
                        .append("consulta_imagenes_cartones.php")
                        .toString();
        String respuesta = restTemplate.getForObject(url,String.class);
        return respuesta;
    }

    
    
}
