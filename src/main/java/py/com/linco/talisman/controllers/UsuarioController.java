/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.EditarCliente;
import py.com.linco.talisman.models.LoginUsuario;
import py.com.linco.talisman.models.RegistroUsuario;
import py.com.linco.talisman.models.RegistroUsuarioV2;
import py.com.linco.talisman.models.UsuarioToken;
import py.com.linco.talisman.models.login.Result;
import py.com.linco.talisman.models.login.ResultCrearCliente;
import py.com.linco.talisman.repositories.UsuarioTokenRepository;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import py.com.linco.talisman.models.RecuperarPassword;
import py.com.linco.talisman.models.UsuarioPass;

/**
 *
 * @author william
 */
@RestController
public class UsuarioController {

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UsuarioTokenRepository usuarioTokenRepository;

    @PostMapping("/api/recuperar-password")
    public Object recuperarPass(@RequestBody RecuperarPassword recuperarPassword){
        String url = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("recuperar_pass.php").toString();
        System.err.println("Email: "+recuperarPassword.getEmail());
        String res = restTemplate.postForObject(url, recuperarPassword, String.class);

        return res;

    }

    @PostMapping("/api/crear-cliente")
    public Object crearUsuarioV2(@RequestBody RegistroUsuarioV2 usuario){
        String url = new StringBuilder()
                        .append(propertiesConfig.getAppBaseUrl())
                        .append("registro_clientes.php")
                        .toString();
        usuario.setPassCheck(usuario.getPassCheck());
        usuario.setNewsletter(false);
        usuario.setEmailCheck(usuario.getEmailCheck());
        try{
            String respuesta = restTemplate.postForObject(url,usuario,String.class);
            Gson gson = new Gson();
            ResultCrearCliente resultCrearCliente = gson.fromJson(respuesta,ResultCrearCliente.class);
            if(resultCrearCliente!=null){
                String identificador = UUID.randomUUID().toString();
                UsuarioToken usuarioToken = new UsuarioToken();
                usuarioToken.setToken(identificador);
                usuarioToken.setApellido(usuario.getClieApe());
                usuarioToken.setEmail(usuario.getClieCorreo());
                usuarioToken.setNombre(usuario.getClieName());
                usuarioToken.setIdUsuarioTalisman(Integer.valueOf(resultCrearCliente.getClienteId()));
                usuarioToken = usuarioTokenRepository.save(usuarioToken);
                return  usuarioToken;

            }
            throw new IllegalArgumentException("Verificar respuesta del servidor de origen");
        }catch (HttpClientErrorException e){
            return new ResponseEntity(e.getResponseBodyAsString(),HttpStatus.BAD_REQUEST);
        }


    }

    @PostMapping("/api/crear-usuario")
    public Object pruebaCrearUsuario(@RequestBody RegistroUsuario registroUsuario) throws JsonProcessingException {
        String url = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("registro_clientes.php").toString();
        registroUsuario.setCorreo(registroUsuario.getNombreUsuario());
        registroUsuario.setToken(propertiesConfig.getToken());
        registroUsuario.setDirIp(propertiesConfig.getDirIp());
        registroUsuario.setPassword(DigestUtils.md5Hex(registroUsuario.getPassword()));
        ObjectMapper objectMapper = new ObjectMapper();
        String js = objectMapper.writeValueAsString(registroUsuario);
        Object res = restTemplate.postForObject(url, registroUsuario, Object.class);
        System.out.println(res);
        return res;
    }
    
    @PostMapping("/api/editar-cliente")
    public Object editarCliente(@RequestBody EditarCliente cliente) {
    	String url = new StringBuilder().append(propertiesConfig.getAppBaseUrl()).append("modificar_cliente.php").toString();
    	cliente.setPassCheck(cliente.getPassCheck());
    	cliente.setNewsletter(false);
    	cliente.setEmailCheck(cliente.getEmailCheck());
    	
        try {
        	String respuesta = restTemplate.postForObject(url, cliente, String.class);
            return respuesta;
        } catch (HttpClientErrorException e) {
        	return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
        }
    }
    
    private Object processResult(Object res,int tipo){
        ArrayList<LinkedHashMap<String,Object>> result =(ArrayList<LinkedHashMap<String,Object>>) res;
        String respuesta = "Ocurrio un error, vuelva a intentar por favor";
        if(result!=null&&result.size()>0){
            LinkedHashMap<String, Object> oLinked = result.get(0);
            Object oStatus =  oLinked.get("status");

            if(oStatus.toString().equals("success")){
                LinkedHashMap<String,Object> oResult =(LinkedHashMap<String, Object>) oLinked.get("usuario");
                if(tipo==1){
                    Integer idUsuario =(Integer) oResult.get("ClienteId");
                    String email = oResult.get("NameUser").toString();
                    String nombre = oResult.get("Nombre").toString();
                    String apellido = oResult.get("Apellido").toString();
                    String identificador = UUID.randomUUID().toString();
                    UsuarioToken usuarioToken = new UsuarioToken();
                    usuarioToken.setToken(identificador);
                    usuarioToken.setApellido(apellido);
                    usuarioToken.setEmail(email);
                    usuarioToken.setNombre(nombre);
                    usuarioToken.setIdUsuarioTalisman(idUsuario);
                    usuarioToken = usuarioTokenRepository.save(usuarioToken);
                    return  usuarioToken;

                }else{
                    Integer idUsuario =(Integer) oResult.get("clienteId");
                    String email = "";
                    String nombre = oResult.get("nombre").toString();
                    String apellido = oResult.get("apellido").toString();
                    String identificador = UUID.randomUUID().toString();
                    UsuarioToken usuarioToken = new UsuarioToken();
                    usuarioToken.setToken(identificador);
                    usuarioToken.setApellido(apellido);
                    usuarioToken.setEmail(email);
                    usuarioToken.setNombre(nombre);
                    usuarioToken.setIdUsuarioTalisman(idUsuario);
                    usuarioToken = usuarioTokenRepository.save(usuarioToken);
                    return  usuarioToken;

                }

            }else{
                respuesta = oLinked.get("respuesta").toString();
            }
        }
        return new ResponseEntity<String>(respuesta, HttpStatus.BAD_REQUEST);
    }
    
    @PostMapping("api/login")
    public Object login(@RequestBody LoginUsuario loginUsuario) throws JsonProcessingException{ 
        String url = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("login_cliente.php").toString();
        loginUsuario.setPassword(org.apache.commons.codec.digest.DigestUtils.md5Hex(loginUsuario.getPassword()));
        loginUsuario.setToken(propertiesConfig.getToken());
        loginUsuario.setDirip(propertiesConfig.getDirIp());
        Gson gson = new Gson();
        String res = restTemplate.postForObject(url, loginUsuario, String.class);
        Type listType = new TypeToken<ArrayList<Result>>(){}.getType();
        List<Result> resultList = gson.fromJson(res,listType);
        if(resultList!=null&&resultList.size()>0){
            Result result = resultList.get(0);
            UsuarioToken usuarioToken = new UsuarioToken();
            String identificador = UUID.randomUUID().toString();
            usuarioToken.setToken(identificador);
            usuarioToken.setEmail(result.getUsuario().getNameUser());
            usuarioToken.setNombre(result.getUsuario().getNombre());
            usuarioToken.setApellido(result.getUsuario().getApellido());
            usuarioToken.setIdUsuarioTalisman(Integer.valueOf(result.getUsuario().getClienteId()));
            usuarioToken = usuarioTokenRepository.save(usuarioToken);
            return usuarioToken;
        }
        throw new IllegalArgumentException("Verificar respuesta del servidor de origen");
    }
    
   

}
