/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.JugadaHelper;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.Cartones;
import py.com.linco.talisman.models.ProximoSorteo;
import py.com.linco.talisman.models.RegistroUsuario;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author william
 */
@RestController
public class RecuperarCartones {
    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JugadaHelper jugadaHelper;
    
    @PostMapping("/api/cartones")
    public Object pruebaCrearUsuario() throws JsonProcessingException, URISyntaxException {
        String urlProximoSorteo = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("proximo_sorteo.php").toString();
        Cartones cartones = new Cartones();
        ProximoSorteo proximoSorteo = new ProximoSorteo();
        proximoSorteo.setToken(propertiesConfig.getToken());
        String respuestaProximo = restTemplate.postForObject(new URI(urlProximoSorteo), proximoSorteo, String.class);

        System.out.println(respuestaProximo);
        JSONArray jsonArray = new JSONArray(respuestaProximo);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        JSONObject objetoRespuesta = jsonObject.getJSONObject("respuesta");

        Integer idJugada = objetoRespuesta.getInt("IdJugada");
        Integer precioCarton = objetoRespuesta.getInt("precioCarton");
        jugadaHelper.setIdJugada(idJugada.toString());
        jugadaHelper.setMontoJugada(precioCarton.toString());


        cartones.setIdJugada(idJugada.toString());



        String url = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("recuperar_cartones.php").toString();
        
        
        cartones.setToken(propertiesConfig.getToken());
        cartones.setDirIp(propertiesConfig.getDirIp());
        cartones.setTipoBusqueda("00");
        cartones.setTerminacion("00");
        ObjectMapper objectMapper = new ObjectMapper();
        String js = objectMapper.writeValueAsString(cartones);
        System.out.println(js);
        String res = restTemplate.postForObject(url, cartones, String.class);

        JSONArray jsonArrayCartones = new JSONArray(res.toString().trim());
        JSONObject jsonObjectDatosJugada = new JSONObject();
        jsonObjectDatosJugada.put("idJugada",idJugada.toString());
        jsonObjectDatosJugada.put("monto",precioCarton.toString());
        jsonArrayCartones.put(1,jsonObjectDatosJugada);
        return jsonArrayCartones.toString();
    }
}
