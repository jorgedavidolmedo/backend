package py.com.linco.talisman.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.models.Faq;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by william on 3/21/18.
 */
@RestController
public class FaqsController {
    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/api/faqs")
    public Faq getSorteoVigente() throws URISyntaxException {
        String url = new StringBuilder()
                .append(propertiesConfig.getAppBaseUrl())
                .append("consulta_faqs.php").toString();


        String response = restTemplate.getForObject(new URI(url),String.class);
        return new Gson().fromJson(response,Faq.class);
    }
}
