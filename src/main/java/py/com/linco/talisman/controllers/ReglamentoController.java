/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.PropertiesConfig;

/**
 *
 * @author william
 */
@RestController
public class ReglamentoController {

	@Autowired
	private PropertiesConfig propertiesConfig;

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/api/reglamento")
	public String getReglamento() {
		String url = new StringBuilder().append(propertiesConfig.getAppBaseUrl()).append("consulta_reglamento.php").toString();
		String respuesta = restTemplate.getForObject(url, String.class);
		return respuesta;
	}

}
