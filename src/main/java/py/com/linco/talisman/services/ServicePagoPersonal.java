package py.com.linco.talisman.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import py.com.linco.talisman.config.JugadaHelper;
import py.com.linco.talisman.config.PropertiesConfig;
import py.com.linco.talisman.repositories.RespuestaBancardRepository;
import py.com.linco.talisman.repositories.UsuarioTokenRepository;

/**
 * Created by william on 1/18/18.
 */
@Service
public class ServicePagoPersonal {
    @Autowired
    private PropertiesConfig propertiesConfig;

    @Autowired
    private RespuestaBancardRepository respuestaBancardRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UsuarioTokenRepository usuarioTokenRepository;

    @Autowired
    private JugadaHelper jugadaHelper;

    @Async
    public void sendPayRequest(String url, HttpEntity<MultiValueMap<String,String>> request) throws InterruptedException {
        String res = restTemplate.postForObject(url,request,String.class);
        System.out.println(Thread.currentThread().getName());
        System.out.println("RESPUESTA DE PERSONAL ES _____");
        System.out.println(res);

    }
}
