package py.com.linco.talisman.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;


/**
 * Created by william on 1/2/18.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class JugadaHelper {

    private String idJugada;
    private String montoJugada;

    public String getIdJugada() {
        return idJugada;
    }

    public void setIdJugada(String idJugada) {
        this.idJugada = idJugada;
    }

    public String getMontoJugada() {
        return montoJugada;
    }

    public void setMontoJugada(String montoJugada) {
        this.montoJugada = montoJugada;
    }

}
