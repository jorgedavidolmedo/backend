/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.linco.talisman.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author william
 */
@Component
@PropertySource("application.properties")
@ConfigurationProperties(prefix = "talisman")
public class PropertiesConfig {
    private String baseUrl;
    private String webUrl;
    private String pruebasUrl;
    private String token;
    private String dirIp;
    private String appUrl;
    private String ventaUrl;
    private String appBaseUrl;

    public String getAppBaseUrl() {
        return appBaseUrl;
    }

    public void setAppBaseUrl(String appBaseUrl) {
        this.appBaseUrl = appBaseUrl;
    }

    private String cartonesUrl;

    public String getCartonesUrl() {
        return cartonesUrl;
    }

    public void setCartonesUrl(String cartonesUrl) {
        this.cartonesUrl = cartonesUrl;
    }

    public String getVentaUrl() {
        return ventaUrl;
    }

    public void setVentaUrl(String ventaUrl) {
        this.ventaUrl = ventaUrl;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }
    
    

    public String getDirIp() {
        return dirIp;
    }

    public void setDirIp(String dirIp) {
        this.dirIp = dirIp;
    }
    
    

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    

    public String getPruebasUrl() {
        return pruebasUrl;
    }

    public void setPruebasUrl(String pruebasUrl) {
        this.pruebasUrl = pruebasUrl;
    }
    
    

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }
    
    
    
    
    
}
