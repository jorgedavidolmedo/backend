package py.com.linco.talisman.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import py.com.linco.talisman.models.RespuestaBancard;

/**
 * Created by william on 11/17/17.
 */
public interface RespuestaBancardRepository extends JpaRepository<RespuestaBancard,Integer> {
}
