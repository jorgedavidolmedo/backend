package py.com.linco.talisman.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import py.com.linco.talisman.models.RespuestaBancard;
import py.com.linco.talisman.models.UsuarioToken;

/**
 * Created by william on 12/22/17.
 */
public interface UsuarioTokenRepository extends JpaRepository<UsuarioToken,Integer> {
    UsuarioToken findByToken(String token);
}
