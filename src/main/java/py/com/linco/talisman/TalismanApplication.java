package py.com.linco.talisman;

import org.json.JSONArray;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class TalismanApplication {

    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("AsyncPagos-");
        executor.initialize();
        return executor;
    }


	public static void main(String[] args) {
                String setProperty = System.setProperty("jsse.enableSNIExtension", "false");
                System.err.println("SETEADO-------");
                System.out.println(setProperty);
		SpringApplication.run(TalismanApplication.class, args);


	}
}
